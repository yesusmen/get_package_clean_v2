import 'package:citv_v1_app/app/ui/pages/login/presentation_page.dart';
import 'package:get/get.dart';
part './routes.dart';

abstract class AppPages {
  static final pages = [
    GetPage(
      name: Routes.HOME,
      page: () => PresentationPage(),
    )
  ];
}
