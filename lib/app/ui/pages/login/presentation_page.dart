import 'package:citv_v1_app/app/controller/presentation_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class PresentationPage extends GetView<PresentationController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text('Presentation_Page')),
        body: SafeArea(child: Text('Presentation_Controller')));
  }
}
